/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vigenere;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Victor
 */
public class VigenereTest {
    
    public VigenereTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void testCrypt() {
        String TP[][];
        TP = new String [][]{
        {"incroyable","toto","bbvfhmtpes"},
        {"incroyable","toto","bbvfhmtpes"},
        {"incroyable","toto","bbvfhmtpes"},
        {"incroyable","toto","bbvfhmtpes"},
        {"incroyable","toto","bbvfhmtpes"},
        };
        int i = 0;
        int j = 2;
        while (i <= 3){ // pour intégrer les 3 première valeurs du tableaux dans les tests 
            i=i+1;   
            String text = TP[i][0];
            String key = TP[i][1];
            if (j == 2){
                String expResult = TP[i][2];
                String result = Vigenere.crypt(text, key);
                assertEquals(expResult, result);
            }
        }
                   
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of decrypt method, of class Vigenere.
     */
    @Test
    public void testDecrypt() {
        System.out.println("decrypt");
        String text = "bbvfhmtpes";
        String key = "toto";
        String expResult = "incroyable";
        String result = Vigenere.decrypt(text, key);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }    
}
